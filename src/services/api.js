import axios from 'axios'

axios.defaults.headers.common['Authorization'] = `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjcsImlkQ3VzdG9tZXIiOjQsIm5hbWUiOm51bGwsInRva2VuSW5mbyI6eyJ0b2tlbiI6ImQ3NTljOTZjODNjZWQ0YzY4NzgxYTg1MGE2ZWExMGJkODBhOTliZGQiLCJlbnYiOiJ3ZWIifSwiaWF0IjoxNTc5MTk5MTg1fQ.X4Oz20GfG3U6hzRdQORPCQ_mkWiRbyMgQwttYHQAoAQ`
axios.defaults.headers.post['Content-Type'] = 'application/json'
const api = axios.create({baseURL: 'http://servidortech.local/api/services'})

export default api