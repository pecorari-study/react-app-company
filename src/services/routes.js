import React from 'react';

import { BrowserRouter, Switch, Route } from 'react-router-dom'

import Home from '../pages/home';
import Lots from '../pages/lots';
import LotDetails from '../pages/lots/details/index.js'
import Sale from '../pages/sale';
import ScheduledSale from '../pages/sale/scheduledSales';
import Removal from '../pages/removal';

const Routes = () => (
    <BrowserRouter>
        <Switch>
           <Route exact path="/" component={Home} />
           <Route exact path="/lots" component={Lots} />
           <Route exact path="/lots/details" component={LotDetails} />
           <Route exact path="/sale" component={Sale} />
           <Route exact path="/scheduledSale" component={ScheduledSale} />
           <Route exact path="/removal" component={Removal} />
        </Switch>
    </BrowserRouter>
)

export default Routes;