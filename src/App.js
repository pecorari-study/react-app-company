import React, { Component } from 'react';
import Routes from './services/routes';

// Components
import Header from './components/Structure/Header/index'
import Sidebar from './components/Structure/Sidebar/index'

export default class App extends Component {
  render () {
    return (
      <div className="App">
        <Header/>
        <div className="app-body">
          <Sidebar/>
          <main className="main">
            <Routes/>
          </main>
        </div>
      </div>
    );
  }
}