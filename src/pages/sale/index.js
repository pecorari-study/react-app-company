import React, { Component } from 'react';

import Report from '../../components/Sale/Report'
import SaleForm from '../../components/Sale/Form'
import { Card } from 'reactstrap';
import './style.scss'


export default class Sale extends Component {

    render() {
        return (
            <div className="sale">
                <SaleForm/>
                <Card>                
                    <Report/>
                </Card>
            </div>
        )
    }
}