import React, { Component } from 'react';
import { Card, CardHeader, CardBody, FormGroup, Label, Input } from 'reactstrap';

import ScheduledSalesForm from '../../../components/Sale/Scheduled/Form'
import './style.scss'

export default class scheduledSales extends Component {

    render() {
        return (
            <div className="scheduledSales">
                <div className="scheduledSalesHeader">
                    <ScheduledSalesForm/>
                </div>
                <div className="scheduledSalesBody">
                    <Card>
                        <CardHeader>30/07/2019</CardHeader>
                        <CardBody>
                            <FormGroup>
                                <Label for="totalAnimals">Total de Animais</Label>
                                <Input type="text" name="animals" id="totalAnimals" placeholder="25" disabled/>
                                <Label for="slaugherhouse">Frigorífico</Label>
                                <Input type="text" name="slaugherhouse" id="slaugherhouse" placeholder="Frigol" disabled/>
                                <Label for="profity">Lucro, R$</Label>
                                <Input type="text" name="profity" id="profity" placeholder="12.000,00" disabled/>
                            </FormGroup>
                        </CardBody>
                    </Card>
                    <Card>
                        <CardHeader>14/08/2019</CardHeader>
                        <CardBody>
                            <FormGroup>
                                <Label for="totalAnimals">Total de Animais</Label>
                                <Input type="text" name="animals" id="totalAnimals" placeholder="20" disabled/>
                                <Label for="slaugherhouse">Frigorífico</Label>
                                <Input type="text" name="slaugherhouse" id="slaugherhouse" placeholder="Frigol" disabled/>
                                <Label for="profity">Lucro, R$</Label>
                                <Input type="text" name="profity" id="profity" placeholder="10.800,00" disabled/>
                            </FormGroup>
                        </CardBody>
                    </Card>
                    <Card>
                        <CardHeader>26/09/2019</CardHeader>
                        <CardBody>
                            <FormGroup>
                                <Label for="totalAnimals">Total de Animais</Label>
                                <Input type="text" name="animals" id="totalAnimals" placeholder="18" disabled/>
                                <Label for="slaugherhouse">Frigorífico</Label>
                                <Input type="text" name="slaugherhouse" id="slaugherhouse" placeholder="Frigol" disabled/>
                                <Label for="profity">Lucro, R$</Label>
                                <Input type="text" name="profity" id="profity" placeholder="8.000,00" disabled/>
                            </FormGroup>
                        </CardBody>
                    </Card>
                </div>
            </div>
        )
    }
}