import React, { Component } from 'react';
import { Card, Button } from 'reactstrap';
import RemovalReport from '../../components/Removal/Report'

import './style.scss'


export default class Home extends Component {

    render() {
        return (
            <div className="removal">
                <form>
                    <Button className="ml-5">Confirmar</Button>
                </form>
                <Card> 
                    <RemovalReport/>
                </Card>
            </div>
        )
    }
}