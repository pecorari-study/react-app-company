import React, { Component } from 'react';

import LotsTable from '../../components/Lots/Table'
import SelectedList from '../../components/Lots/SelectedList'
import './style.scss'
import { connect } from 'react-redux'
import { fetchLots } from '../../store/actions/lot'

export class Lots extends Component {
  componentDidMount () {
    this.props.fetchLots()
  }
  render() {
    const {lots } = this.props
    return (
      <div className="lots animated fadeIn">
        <SelectedList/>
        <div className="lots-container">
          <LotsTable lots={lots}/>        
        </div>
      </div>
    );
  }
}

const MapStateToProps = (state) => {
  return {
    lots: state.lots
  };
};

const MapDispatchToProps = (dispatch) => {
  return {
    fetchLots: () => dispatch (fetchLots)
  };
};

export default connect(MapStateToProps, MapDispatchToProps)(Lots);