import React, { Component } from 'react';
import FeedlotAnimalsCard from '../../components/Feedlot/AnimalsCard'
import FeedlotProfityCard from '../../components/Feedlot/ProfityCard'
import FeedlotPerformanceCard from '../../components/Feedlot/PerformanceCard'
import SystemEvents from '../../components/System/Events'

import './style.scss'

export default class Home extends Component {

render() {

return (
    <div className="home animated fadeIn">
        <SystemEvents/>
        <div className="home-container">
            <div className="home-header">
                <FeedlotAnimalsCard/>
                <FeedlotProfityCard/>
            </div>
            <FeedlotPerformanceCard/>
        </div>
    </div>
    )
}
}



