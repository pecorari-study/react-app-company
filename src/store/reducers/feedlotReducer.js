import {FETCH_FEEDLOTS} from '../actions/feedlots';

const feedlotReducer = (state = {}, {type, payload}) => {
    switch(type) {
        case FETCH_FEEDLOTS:
            return payload
        
        default:
            return state

    };
};

export default feedlotReducer;