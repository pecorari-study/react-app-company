import {FETCH_LOTS} from '../actions/lot';

const lotReducer = (state = {}, {type, payload}) => {
    switch(type) {
        case FETCH_LOTS:
            return payload;
        default:
        return state;
    };
};

export default lotReducer;