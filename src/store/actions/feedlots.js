import api from '../../services/api'

export const FETCH_FEEDLOTS = "FETCH_FEEDLOTS";

export const fetchFeedlots = (dispatch) => {
    api.post('/feedlot/get', {
        query: {
            filters: []
        }
    }) 
    .then(res => dispatch({type: FETCH_FEEDLOTS, payload: res.data}))
};