import api from '../../services/api'

export const FETCH_LOTS = 'FETCH_LOTS'

export const fetchLots = (dispatch) => {
    api.post('/feedlot/lot/get', {
        query: {
            filters: []
        }
    })
    .then(res => dispatch({type: FETCH_LOTS, payload: res.data}));
};