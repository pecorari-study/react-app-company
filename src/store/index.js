import {createStore, combineReducers, compose, applyMiddleware} from 'redux';

import feedlotReducer from './reducers/feedlotReducer';
import lotReducer from './reducers/lotReducer';

import thunk from 'redux-thunk';

const middleware = [thunk];

const allReducers = combineReducers({feedlots: feedlotReducer, lots: lotReducer});

const initialState = {
    feedlots: [],
    lots: []
};

const store = createStore(allReducers, initialState, compose( applyMiddleware(...middleware), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()));
export default store;
