import React, { useState } from 'react';
import classnames from 'classnames';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import { FaCartPlus, FaStickerMule, FaDollarSign, FaRegChartBar, FaChartArea, FaWeightHanging } from 'react-icons/fa'
import LotCycleCard from '../Sale/LotCycleCard'
import Charts from './Details/Charts';
import AnimalsTable from '../Animals/Table'
import SelectedLotsList from './SelectedList'

const Operations = () => {
  const [activeTab, setActiveTab] = useState('1');  
  const toggle = tab => {
    if(activeTab !== tab) setActiveTab(tab);
  }
    return (
        <div className="lot-details">
          <Nav tabs className="operations">
            <NavItem>
              <NavLink
                className={classnames({ active: activeTab === '1' })}
                onClick={() => { toggle('1'); }}>
                <FaCartPlus/>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: activeTab === '2' })}
                onClick={() => { toggle('2'); }}>
                <FaStickerMule/>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: activeTab === '3' })}
                onClick={() => { toggle('3'); }}>
                <FaDollarSign/>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: activeTab === '4' })}
                onClick={() => { toggle('4'); }}>
                <FaRegChartBar/>
              </NavLink>
            </NavItem>        
            <NavItem>
              <NavLink
                className={classnames({ active: activeTab === '5' })}
                onClick={() => { toggle('5'); }}>
                <FaChartArea/>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: activeTab === '6' })}
                onClick={() => { toggle('6'); }}>
                <FaWeightHanging/>
              </NavLink>
            </NavItem>      
          </Nav> 
          <TabContent activeTab={activeTab}>
            <h1>Lote 01</h1>
            <TabPane tabId="1">
              <Charts/>
              <div className="sales-cards">
                  <LotCycleCard/>
                  <LotCycleCard/>
              </div>
            </TabPane>
            <TabPane tabId="2">
               <SelectedLotsList/> 
              <AnimalsTable/>
            </TabPane>
          </TabContent>
        </div>
    );
}

export default Operations