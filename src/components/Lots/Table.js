import React from 'react';
import { Link } from 'react-router-dom'
import { Table } from 'reactstrap';

const LotsTable = (props) => {
  const { lots } = props
  return (
    <Table responsive className="lots-table">
        <thead>
          <tr>
            <th>#</th>
            <th>ID Lote</th>
            <th>Data entrada</th>
            <th>Qtd Animais</th>
            <th>Dias Confinamento</th>
            <th>Método Tradicional, R$</th>
            <th>Beeftrader, R$</th>
            <th>N Vendas</th>
            <th>Detalhes</th>
          </tr>
        </thead>
        <tbody>
          {lots.map(lot => (
            <tr>
              <th scope="row">1</th>
              <td>{lot.lotDescription}</td>
              <td>01/03/2019</td>
              <td>{lot.activeAnimalsCount + lot.disabledAnimalsCount}</td>
              <td>124</td>
              <td>10.000,00</td>
              <td>15.000,00</td>
              <td>1</td>
              <td><Link to={`/lots/details`}></Link>+</td>
            </tr>
          ))}           
        </tbody>
      </Table> 
  );
}

export default LotsTable