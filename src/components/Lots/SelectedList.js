import React, { useState } from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, Card, CardTitle, CardText } from 'reactstrap';
import { FaCheckSquare, FaCalendarDay, FaTimesCircle } from 'react-icons/fa'

const SelectedList = () => {
  const [collapsed, setCollapsed] = useState(true);

  const toggleNavbar = () => setCollapsed(!collapsed);

  return (
      <Navbar className="app-aside lots-selected-list" color="faded" light>
        <NavbarToggler onClick={toggleNavbar}>
          <NavbarBrand href="/"><FaCheckSquare/></NavbarBrand>
          <Collapse isOpen={!collapsed} navbar>
              <Nav navbar>
                  <h5>Selecionados</h5>
              <NavItem>
                  <Card body>
                      <CardTitle>Lote 1</CardTitle>
                      <CardText>25 animais</CardText>
                  </Card>
                  <Card body>
                      <CardTitle>Lote 2</CardTitle>
                      <CardText>20 animais</CardText>
                  </Card>
                  <Card body>
                      <CardTitle>Lote 3</CardTitle>
                      <CardText>5 animais</CardText>
                  </Card>
              </NavItem>
              <NavItem>
                  <h5>Agendar venda <FaCalendarDay/></h5>
              </NavItem>
              <NavItem>
                  <h5>Remover <FaTimesCircle/></h5>
              </NavItem>
              </Nav>
          </Collapse>
        </NavbarToggler>
      </Navbar>
  );
}

export default SelectedList;