import React from 'react';

// Charts
import ProfityChart from './ProfityChart'
import WeighingChart from './WeighingChart'
import WeighingDayChart from './WeighingDayChart'

const Charts = () =>  {
  return (
    <div className="charts">
      <ProfityChart/>
      <WeighingChart/>
      <WeighingDayChart/>
    </div>
  );
}

export default Charts