import React from "react";
import { VictoryChart, VictoryGroup, VictoryBar } from "victory";
import { FormGroup, Label } from 'reactstrap';

const WeighingDayChart = () => {
  return (
    <FormGroup className="profity-day-chart">
        <Label for="profity">Peso, Kg</Label>
        <VictoryChart>
            <VictoryGroup offset={20}
                colorScale={"qualitative"}
            >
                <VictoryBar
                data={[{ x: 1, y: 1 }, { x: 2, y: 2 }, { x: 3, y: 5 }]}
                />
                <VictoryBar
                data={[{ x: 1, y: 2 }, { x: 2, y: 1 }, { x: 3, y: 7 }]}
                />
            </VictoryGroup>

        </VictoryChart>
    </FormGroup>
  );
}

export default WeighingDayChart