import React from "react";
import { VictoryPolarAxis, VictoryTheme } from "victory";
import { FormGroup, Label } from 'reactstrap';

const WeighingChart = () => {
  return (
    <FormGroup className="weighing-chart">
        <Label for="profity">Peso, Kg</Label>
        <svg width={400} height={400}>
          <VictoryPolarAxis
              width={400}
              height={400}
              theme={VictoryTheme.material}
              standalone={false}
          />
          <VictoryPolarAxis dependentAxis
              width={400}
              height={400}
              domain={[0, 10]} />
        </svg>
    </FormGroup>
  );
}

export default WeighingChart