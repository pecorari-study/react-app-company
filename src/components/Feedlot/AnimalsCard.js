import React from 'react';
import { Card, Button, CardHeader, CardFooter, CardBody,
    CardTitle, CardText } from 'reactstrap';
  
  import { FaStickerMule, FaQuestionCircle } from 'react-icons/fa';
  
  const AnimalsCard = () => {
  return (
    <Card>
        <CardHeader tag="h3"><Button><FaStickerMule/> Animais</Button><FaQuestionCircle/></CardHeader>
        <CardBody>
            <CardTitle>Special Title Treatment</CardTitle>
            <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
        </CardBody>
        <CardFooter className="text-muted">Distribuídos em 8 baias</CardFooter>
    </Card>
  )
  }
  export default AnimalsCard;