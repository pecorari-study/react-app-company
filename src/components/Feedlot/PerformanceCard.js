import React from 'react';
import { Card, Button, CardHeader, CardBody,
    CardTitle, CardText } from 'reactstrap';
  
  import { FaChartLine, FaQuestionCircle } from 'react-icons/fa';
  
  const PerformanceCard = () => {
  return (
    <Card>
        <CardHeader tag="h3"> <Button><FaChartLine/> Performance Zootécnica</Button><FaQuestionCircle/></CardHeader>
        <CardBody>
            <CardTitle>Special Title Treatment</CardTitle>
            <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
        </CardBody>
    </Card>
)
  }
  export default PerformanceCard;