import React from 'react';
import { Card, Button, CardHeader, CardFooter, CardBody,
  CardTitle, CardText } from 'reactstrap';

import { FaQuestionCircle } from 'react-icons/fa';

const ProfityCard = () => {
return (
    <Card>
        <CardHeader tag="h3"><Button>$ Lucro, R$</Button><FaQuestionCircle/></CardHeader>
        <CardBody>
            <CardTitle>Special Title Treatment</CardTitle>
            <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
        </CardBody>
        <CardFooter className="text-muted">16.000,00 superior comparado ao método tradicional</CardFooter>
    </Card>
)
}
export default ProfityCard;