import React from 'react';
import { CardBody, CardTitle } from 'reactstrap';

import GeralIndexesTable from './GeralIndexesTable'
import SelectedAnimalsTable from './SelectedAnimalsTable'


const Report = () => {
    return (
        <CardBody className="sale-report">
            <CardTitle>Índices Gerais</CardTitle>
            <GeralIndexesTable/>
            <SelectedAnimalsTable/>
        </CardBody>
    )
}

export default Report