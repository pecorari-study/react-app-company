import React from 'react';
import { Table } from 'reactstrap';


const SelectedAnimalsTable = () => {
    return (                
        <Table className="Selected-animals-table">
            <thead>
                <tr>
                    <th>Brinco</th>
                    <th>Peso, kg</th>
                    <th>Lucro, R$</th>
                    <th>Lucratividade, R$/@</th>
                    <th>Ganho Médio Diário, Kg</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Peso Médio Atual, kg</td>
                    <td>Peso Médio Saída, kg</td>
                    <td>Ganho Médio Diário, kg</td>
                    <td>Lucro Acumulado Atual, R$</td>
                </tr>
                <tr>
                    <td>85739-6</td>
                    <td>512</td>
                    <td>6800,00</td>
                    <td>45,00</td>
                    <td>1,42</td>
                </tr>
                <tr>
                    <td>85739-6</td>
                    <td>512</td>
                    <td>6800,00</td>
                    <td>45,00</td>
                    <td>1,42</td>
                </tr>
                <tr>
                    <td>85739-6</td>
                    <td>512</td>
                    <td>6800,00</td>
                    <td>45,00</td>
                    <td>1,42</td>
                </tr>
                <tr>
                    <td>85739-6</td>
                    <td>512</td>
                    <td>6800,00</td>
                    <td>45,00</td>
                    <td>1,42</td>
                </tr>
                <tr>
                    <td>85739-6</td>
                    <td>512</td>
                    <td>6800,00</td>
                    <td>45,00</td>
                    <td>1,42</td>
                </tr>
                <tr>
                    <td>85739-6</td>
                    <td>512</td>
                    <td>6800,00</td>
                    <td>45,00</td>
                    <td>1,42</td>
                </tr>
            </tbody>
        </Table>
    )      
}

export default SelectedAnimalsTable