import React from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

const SaleForm = (props) => {
  return (
    <Form inline className="sale-form">
      <FormGroup className="mb-2 mr-sm-5 mb-sm-0">
      <Label for="slaugherhouseSelect" className="mr-2">Selecionar data</Label>
        <Input type="date" name="date"/>
      </FormGroup>
      <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
        <Label for="slaugherhouseSelect" className="mr-2">Selecionar frigorífico </Label>
        <Input type="select" name="select" id="slaugherhouseSelect">
          <option>Frigol</option>
          <option>Ageleli</option>
          <option>JBS</option>
          <option>Valor Pessoal</option>
        </Input>
      </FormGroup>
      <Button className="ml-5">Agendar</Button>
    </Form>
  );
}

export default SaleForm;