import React from 'react';
import { Table } from 'reactstrap';


const GeralIndexesTable = () => {
        return (
                <Table className="Geral-indexes-table">
                    <thead>
                        <tr>
                            <th>Total de Animais</th>
                            <th>55</th>
                        </tr>
                    </thead>
                    <tbody>
                        <td>
                            <tr>Peso Médio Atual, kg</tr>
                            <tr>Peso Médio Saída, kg</tr>
                            <tr>Ganho Médio Diário, kg</tr>
                            <tr>Lucro Acumulado Atual, R$</tr>
                        </td>
                        <td>
                            <tr>457</tr>
                            <tr>548</tr>
                            <tr>1.42</tr>
                            <tr>18.490,00</tr>
                            <tr>20.480,00</tr>
                        </td>
                    </tbody>
                </Table>
        )
}

export default GeralIndexesTable