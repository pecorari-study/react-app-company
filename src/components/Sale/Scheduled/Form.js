import React from 'react';
import { Button, Form } from 'reactstrap';

const ScheduledSaleForm = (props) => {
  return (
    <Form inline className="sale-form">
      <Button className="ml-5">Cancelar agendamento</Button>
      <Button className="ml-5">Confirmar agendamento</Button>
    </Form>
  );
}

export default ScheduledSaleForm;