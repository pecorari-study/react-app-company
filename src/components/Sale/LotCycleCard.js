import React from "react";
import { Card, CardHeader, CardBody, Table } from 'reactstrap';
import { FaDollarSign, FaWeightHanging, FaChartLine } from 'react-icons/fa'

const LotCycleCard = () => {
  return (
    <Card className="Lot-cycle-card">
    <CardHeader>1 Venda</CardHeader>
    <CardBody>
        <Table size="sm">
            <tbody>
              <tr>
                  <td><FaDollarSign/></td>
                  <td>12</td>
                  <td>15</td>
                  <td>21</td>
              </tr>
              <tr>
                <td><FaWeightHanging/></td>
                <td>489</td>
                <td>497</td>
                <td>502</td>                    
              </tr>
              <tr>
                <td><FaChartLine/></td>
                <td>1,1</td>
                <td>1,15</td>
                <td>1,32</td>
              </tr>
            </tbody>
        </Table>
    </CardBody>
  </Card>
  );
}

export default LotCycleCard;