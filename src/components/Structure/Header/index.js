import React, { Component } from 'react';
import { FaArrowLeft } from 'react-icons/fa';
import {connect} from 'react-redux'
import {fetchFeedlots} from './../../../store/actions/feedlots'

//Components
import { FormGroup, Input } from 'reactstrap';


class Header extends Component {

	componentDidMount() {
		this.props.fetchFeedlots();
	}

	render() {
		const { feedlots } = this.props
		return (
			<header className="app-header">
				<a href="/">
					<FaArrowLeft className="mr-1"/>
					Animais
				</a>
				<FormGroup>
					<Input type="select" name="feedlot" id="feedlot">
						{feedlots.map(feedlot => (
							<option key = {feedlot.feedlotId}>{feedlot.feedlotName}</option>
						))};		
					</Input>
				</FormGroup>
			</header>
		)
	}
}

const MapStateToProps = (state) => {
	return {
		feedlots: state.feedlots
	};
};

const MapDispatchToProps = (dispatch) => {
	return {
		fetchFeedlots: () => dispatch(fetchFeedlots)
	};
};

export default connect(MapStateToProps, MapDispatchToProps)(Header);