import React, { useState } from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import { FaHome, FaWpforms, FaPen, FaWrench, FaHistory } from 'react-icons/fa';

const Sidebar = (props) => {
  const [collapsed, setCollapsed] = useState(true);

  const toggleNavbar = () => setCollapsed(!collapsed);

  return (
    <Navbar className="app-sidebar" color="faded" light>
        <NavbarBrand href="/" className="mr-auto"/>
        <NavbarToggler onClick={toggleNavbar} className="mr-2">
          <Collapse isOpen={collapsed} navbar>
            <Nav navbar className="sidebarClosed">
              <NavItem>
                <NavLink href="/"><FaHome/></NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/lots"><FaWpforms/></NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/lots/details"><FaPen/></NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/setup"><FaWrench/></NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/history"><FaHistory/></NavLink>
              </NavItem>
            </Nav>
          </Collapse>
          <Collapse isOpen={!collapsed} navbar>
            <Nav navbar className="sidebarOpenned"> 
              <NavItem>
                <NavLink href="/"><FaHome/> Home</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/lots"><FaWpforms/>  Lotes</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/lots/details"><FaPen/> Edição</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/setup"><FaWrench/> Configurações</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/history"><FaHistory/> Histórico</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </NavbarToggler>
      </Navbar>
  );
}

export default Sidebar;