import React from 'react';
import { Table, CardBody, CardTitle } from 'reactstrap';


const RemovalReport = () => {
    return (
        <CardBody className="removal-report">
                    <CardTitle>Animais Selecionados</CardTitle>
                    <Table>
                        <thead>
                            <tr>
                                <th>Brinco</th>
                                <th>Peso real, kg</th>
                                <th>Peso predito, kg</th>
                                <th>Lucro real, R$</th>
                                <th>Lucro predito, R$</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>85739-6</td>
                                <td>500</td>
                                <td>512</td>
                                <td>6500,00</td>
                                <td>6800,00</td>
                            </tr>
                            <tr>
                                <td>85739-6</td>
                                <td>500</td>
                                <td>512</td>
                                <td>6500,00</td>
                                <td>6800,00</td>
                            </tr>
                            <tr>
                                <td>85739-6</td>
                                <td>500</td>
                                <td>512</td>
                                <td>6500,00</td>
                                <td>6800,00</td>
                            </tr>
                            <tr>
                                <td>85739-6</td>
                                <td>500</td>
                                <td>512</td>
                                <td>6500,00</td>
                                <td>6800,00</td>
                            </tr>
                            <tr>
                                <td>85739-6</td>
                                <td>500</td>
                                <td>512</td>
                                <td>6500,00</td>
                                <td>6800,00</td>
                            </tr>
                            <tr>
                                <td>85739-6</td>
                                <td>500</td>
                                <td>512</td>
                                <td>6500,00</td>
                                <td>6800,00</td>
                            </tr>
                        </tbody>
                        </Table>
                </CardBody>
    )    
}

export default RemovalReport