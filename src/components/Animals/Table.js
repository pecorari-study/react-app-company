import React from 'react';
import { Table } from 'reactstrap';
import TableForm from './TableForm'

import { FaFilter, FaPen } from 'react-icons/fa'

const AnimalsTable = () => {
  return (
    <div className="animalTable">
        <TableForm/>
        <Table responsive>
            <thead>
                <tr>
                    <th><FaFilter/></th>
                    <th>Brinco</th>
                    <th>Gênero</th>
                    <th>Raça</th>
                    <th>Peso Real, kg</th>
                    <th>Ganho de Peso, Kg</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row"><img src="" alt="animalImg"/></th>
                    <td>85739-6</td>
                    <td>MI</td>
                    <td>Nelore</td>
                    <td>550</td>
                    <td>1.45</td>
                    <td><FaPen/></td>
                </tr>
                <tr>
                    <th scope="row"><img src=""  alt="animalImg"/></th>
                    <td>85739-6</td>
                    <td>MI</td>
                    <td>Nelore</td>
                    <td>550</td>
                    <td>1.45</td>
                    <td><FaPen/></td>
                </tr>
                <tr>
                    <th scope="row"><img src=""  alt="animalImg"/></th>
                    <td>85739-6</td>
                    <td>MI</td>
                    <td>Nelore</td>
                    <td>550</td>
                    <td>1.45</td>
                    <td><FaPen/></td>
                </tr>
            </tbody>
        </Table>
    </div>
  );
}

export default AnimalsTable