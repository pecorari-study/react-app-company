import React from 'react';
import { Form, FormGroup, Label, Input, Button, ButtonGroup } from 'reactstrap';

const TableForm = () => {
    return (
        <Form className="table-form">
            <FormGroup>
            <ButtonGroup>
                <Button>1 Venda</Button>
                <Button>2 Venda</Button>
                <Button>3 Venda</Button>
            </ButtonGroup>
            <label>1 venda 03/02</label>
            <label>2 venda 15/05</label>
            </FormGroup>
            <FormGroup check inline>
            <Label check>
                <Input type="checkbox" /> Mostrar Ativos
            </Label>
            </FormGroup>
            <FormGroup check inline>
            <Label check>
                <Input type="checkbox" /> Mostrar Inativos
            </Label>
            </FormGroup>
        </Form>
    );
}

export default TableForm