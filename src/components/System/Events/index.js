import React, { useState } from 'react';
import { Nav, NavItem, Card, CardTitle, CardText, Collapse, Navbar, NavbarToggler, NavbarBrand } from 'reactstrap';

import { FaRegCalendarAlt } from 'react-icons/fa'

const SystemEvents = () => {

  const [collapsed, setCollapsed] = useState(true);

  const toggleNavbar = () => setCollapsed(!collapsed);

  return (
    <Navbar className="app-aside system-events" color="faded" light>
      <NavbarToggler onClick={toggleNavbar}>
        <NavbarBrand href="/"><FaRegCalendarAlt/></NavbarBrand>
        <Collapse isOpen={!collapsed} navbar>
          <Nav navbar>
            <h5> Eventos</h5>
            <NavItem>
              <Card body>
                <CardTitle>15/07/2019</CardTitle>
                <CardText>Venda 1 - Lote 1</CardText>
                <CardText>Angeleli</CardText>
                <CardText>25 animais</CardText>
                <CardText>R$ 15.000,00</CardText>
              </Card>
              <Card body>
                <CardTitle>15/07/2019</CardTitle>
                <CardText>Venda 1 - Lote 1</CardText>
                <CardText>Angeleli</CardText>
                <CardText>25 animais</CardText>
                <CardText>R$ 15.000,00</CardText>
              </Card>
              <Card body>
                <CardTitle>15/07/2019</CardTitle>
                <CardText>Venda 1 - Lote 1</CardText>
                <CardText>Angeleli</CardText>
                <CardText>25 animais</CardText>
                <CardText>R$ 15.000,00</CardText>
              </Card>
            </NavItem>
          </Nav>
        </Collapse>
      </NavbarToggler>
    </Navbar> 
  );
}

export default SystemEvents;